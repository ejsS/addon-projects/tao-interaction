define(['IMSGlobal/jquery_2_1_1', 'OAT/util/html'], function($, html){

    function renderUrl(id, $container, config){

        var url = config.url || "",
            $iframe = $container.find('#myejss');
        
        //ensure that renderChoices() is idempotent
        $iframe.empty();

		$iframe.attr('src', url);
    }

    function renderHeight(id, $container, config){

        var height = config.height || "",
            $container = $container.find('.ejsScontainer');
        
		$container.height(height);
    }

    return {
        render : function(id, container, config, assetManager){

            var $container = $(container);

            renderUrl(id, $container, config);
			renderHeight(id, $container, config);
            
            //render rich text content in prompt
            html.render($container.find('.prompt'));
        },
        renderUrl : function(id, container, config){
            renderUrl(id, $(container), config);
        },
        renderHeight : function(id, container, config){
            renderHeight(id, $(container), config);
        }
    };
});