<div class="panel">
    <label for="" class="has-icon">{{__ "URL of EjsS simulation"}}</label>
    <span class="icon-help tooltipstered" data-tooltip="~ .tooltip-content:first" data-tooltip-theme="info"></span>
    <div class="tooltip-content">{{__ 'URL of EjsS simulation.'}}</div>

    <input type="text" 
           name="url" 
           value="{{url}}">

    <label for="" class="has-icon">{{__ "Height of EjsS simulation"}}</label>
    <span class="icon-help tooltipstered" data-tooltip="~ .tooltip-content:first" data-tooltip-theme="info"></span>
    <div class="tooltip-content">{{__ 'Height of EjsS simulation. For example, 60vh or 500px'}}</div>

    <input type="text" 
           name="height" 
           value="{{height}}">
		   
</div>