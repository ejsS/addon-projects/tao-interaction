define([
    'taoQtiItem/qtiCreator/widgets/interactions/customInteraction/Widget',
    'ejsSInteraction/creator/widget/states/states'
], function(Widget, states){

    var ejsSInteractionWidget = Widget.clone();

    ejsSInteractionWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    return ejsSInteractionWidget;
});