define([
    'taoQtiItem/qtiCreator/widgets/states/factory',
    'taoQtiItem/qtiCreator/widgets/interactions/states/Question',
    'taoQtiItem/qtiCreator/widgets/helpers/formElement',
    'taoQtiItem/qtiCreator/editor/simpleContentEditableElement',
    'taoQtiItem/qtiCreator/editor/containerEditor',
    'tpl!ejsSInteraction/creator/tpl/propertiesForm',
    'lodash',
    'jquery'
], function(stateFactory, Question, formElement, simpleEditor, containerEditor, formTpl, _, $){

    var ejsSInteractionStateQuestion = stateFactory.extend(Question, function(){

        var $container = this.widget.$container,
            $prompt = $container.find('.prompt'),
            interaction = this.widget.element;

        containerEditor.create($prompt, {
            change : function(text){
                interaction.data('prompt', text);
                interaction.updateMarkup();
            },
            markup : interaction.markup,
            markupSelector : '.prompt',
            related : interaction
        });

        simpleEditor.create($container, '.ejsS-label-min', function(text){
            interaction.prop('label-min', text);
        });

        simpleEditor.create($container, '.ejsS-label-max', function(text){
            interaction.prop('label-max', text);
        });

    }, function(){

        var $container = this.widget.$container,
            $prompt = $container.find('.prompt');

        simpleEditor.destroy($container);
        containerEditor.destroy($prompt);
    });

    ejsSInteractionStateQuestion.prototype.initForm = function(){		
	
        var _widget = this.widget,
            $form = _widget.$form,
            interaction = _widget.element,
            response = interaction.getResponseDeclaration(),
            url = interaction.prop('url') || "",
			height = interaction.prop('height') || "";

        //render the form using the form template
        $form.html(formTpl({
            serial : response.serial,
            url : [url],
			height: [height]
        }));

        //init form javascript
        formElement.initWidget($form);
		
        // init data change callbacks
        formElement.setChangeCallbacks($form, interaction, {
            url : function(interaction, value){
                //update the pci property value:
                interaction.prop('url', value);
                
                //trigger change event:
                interaction.triggerPci('urlchange', [value]);
            },
			height : function(interaction, value){
                //update the pci property value:
                interaction.prop('height', value);
                
                //trigger change event:
                interaction.triggerPci('heightchange', [value]);
            } 
        });
    };

    return ejsSInteractionStateQuestion;
});
