# TAO custom interaction for EjsS Simulations

## Getting Started

Steps available at https://userguide.taotesting.com/3.2/interactions/portable-custom-interactions.html

## Author

* **Félix J. García-Clemente** - [CV] (https://webs.um.es/fgarcia/)


